package com.reef.mobile.automation.ui.base;

public enum Grid {

    // Local appium server
    NONE(""),
    LOCAL("http://localhost:4723/wd/hub"),
    // ACD Manager
    //REMOTE("http://localhost:4723/wd/hub");
    // Selenium hub
    REMOTE("http://172.17.0.1:4444/wd/hub");


    public final String url;

    Grid(final String gridUrl) {
        this.url = gridUrl;
    }
}
