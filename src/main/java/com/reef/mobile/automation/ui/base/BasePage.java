package com.reef.mobile.automation.ui.base;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@Slf4j
@Getter
public abstract class BasePage {

    protected final EventFiringWebDriver driver;
    private final WebDriverWait webDriverWait;

    public BasePage(final BaseDriver baseDriver) {
        this.driver = baseDriver.getDriver();
        this.webDriverWait = new WebDriverWait(driver,10);
    }

    /**
     * Wait for element to be displayed and send keys to it
     * @param by element on the page
     * @param keysToSend value which we send to the element
     */
    public void waitAndSendKeys(final By by, final String keysToSend) {
        waitForElementToBeDisplayed(by);
        driver.findElement(by).sendKeys(keysToSend);
    }

    /**
     * Wait for element to be displayed and click on it
     * @param by element to be waited for and clicked
     */
    public void waitAndClick(final By by) {
        waitForElementToBeDisplayed(by);
        driver.findElement(by).click();
    }

    /**
     * Wait for element to be displayed
     * @param by element to be waited for
     * @return true if element is displayed otherwise false
     */
    public boolean waitForElementToBeDisplayed(final By by) {
        return getWebDriverWait().until(ExpectedConditions.visibilityOfElementLocated(by)).isDisplayed();
    }

    /**
     * Get element text value
     * @param by element which we take text from
     * @return text value
     */
    public String getElementText(final By by) {
        return driver.findElement(by).getText();
    }
}