package com.reef.mobile.automation.ui.base;

import com.reef.mobile.automation.exceptions.MobileAutomationException;
import com.reef.mobile.automation.storage.Storage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import static io.appium.java_client.remote.MobileCapabilityType.*;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Objects;

@Slf4j
@Component
@Setter
@Getter
@Scope("cucumber-glue")
public class BaseDriver {

    private static final String INITIALIZE_DRIVER = "Initializing driver.";

    @Autowired
    private Storage storage;

    private Platform platform;
    private boolean useRemoteWebDriver;
    private String remoteWebDriverUrl = null;
    private String localWebDriverUrl = null;

    private DesiredCapabilities desiredCapabilities = new DesiredCapabilities();

    private final ThreadLocal<EventFiringWebDriver> driver = new ThreadLocal<>();

    /**
     * Setup method with selenium grid
     * @param grid Selenium Grid, possible values "none", "local", "remote" (case insensitive)
     */
    public BaseDriver(@Value("${grid:remote}") final String grid) {
        if (getGrid(grid).equals(Grid.NONE) || getGrid(grid).equals(Grid.LOCAL)) {
            this.useRemoteWebDriver = false;
            this.localWebDriverUrl = Grid.LOCAL.url;
        } else {
            this.useRemoteWebDriver = true;
            this.remoteWebDriverUrl = Grid.REMOTE.url;
        }
    }

    /**
     * Get driver
     *
     * @return {@link EventFiringWebDriver}
     */
    public EventFiringWebDriver getDriver() {
        return driver.get();
    }

    /**
     * Initialize Android WebDriver
     */
    public void initializeAndroidWebDriver() {
        setPlatform(Platform.ANDROID);
        setAndroidDesiredCapabilities();
        if (this.useRemoteWebDriver) {
            log.info("Using Remote WebDriver");
            try {
                final RemoteWebDriver remoteWebDriver = new AndroidDriver<>(new URL(remoteWebDriverUrl), getDesiredCapabilities());
                remoteWebDriver.setFileDetector(new LocalFileDetector());
                log.info("Initializing remote webdriver with url {} and for platform {}.", remoteWebDriverUrl, platform.toString());
                driver.set(new EventFiringWebDriver(remoteWebDriver));
            } catch (final MalformedURLException e) {
                final String msg = "Error while initializing remote webdriver with url: " + remoteWebDriverUrl.toUpperCase();
                log.error(msg, e);
                throw new MobileAutomationException(msg, e);
            }
        } else {
            log.info(INITIALIZE_DRIVER);
            try {
                driver.set(new EventFiringWebDriver(new AndroidDriver<>(new URL(localWebDriverUrl), getDesiredCapabilities())));
            } catch (final MalformedURLException e) {
                final String msg = "Error while initializing local webdriver with url: " + localWebDriverUrl.toUpperCase();
                log.error(msg, e);
                throw new MobileAutomationException(msg, e);
            }
        }
    }

    /**
     * Initialize IOS WebDriver
     */
    public void initializeIOSWebDriver() {
        setPlatform(Platform.IOS);
        if (this.useRemoteWebDriver) {
            log.info("Using Remote WebDriver");
            setIOSRemoteDesiredCapabilities();
            try {
                final RemoteWebDriver remoteWebDriver = new IOSDriver<>(new URL(remoteWebDriverUrl), getDesiredCapabilities());
                remoteWebDriver.setFileDetector(new LocalFileDetector());
                log.info("Initializing remote webdriver with url {} and for platform {}.", remoteWebDriverUrl, platform.toString());
                driver.set(new EventFiringWebDriver(remoteWebDriver));
            } catch (final MalformedURLException e) {
                final String msg = "Error while initializing remote webdriver with url: " + remoteWebDriverUrl.toUpperCase();
                log.error(msg, e);
                throw new MobileAutomationException(msg, e);
            }
        } else {
            setIOSLocalDesiredCapabilities();
            log.info(INITIALIZE_DRIVER);
            try {
                driver.set(new EventFiringWebDriver(new IOSDriver<>(new URL(localWebDriverUrl), getDesiredCapabilities())));
            } catch (final MalformedURLException e) {
                final String msg = "Error while initializing local webdriver with url: " + localWebDriverUrl.toUpperCase();
                log.error(msg, e);
                throw new MobileAutomationException(msg, e);
            }
        }
    }

    /**
     * Get android Application file
     * @return application file {@link File}
     */
    private File getAndroidAppFile() {
        try {
            return Paths.get(Objects.requireNonNull(getClass()
                    .getClassLoader().getResource("apps/app-uat-debug.apk.zip"))
                    .toURI()).toFile();
        } catch (URISyntaxException e) {
            throw new MobileAutomationException("App path could not be found, please check! {}", e.toString());
        }
    }

    /**
     * Get ios Application file
     * @return application file {@link File}
     */
    private File getIOSAppFile () {
        try {
            return Paths.get(Objects.requireNonNull(getClass()
                    .getClassLoader().getResource("apps/REEF Mobile.app.zip"))
                    .toURI()).toFile();
        } catch (URISyntaxException e) {
            throw new MobileAutomationException("App path could not be found, please check! {}", e.toString());
        }
    }

    /**
     * Set Android Desired Capabilities
     */
    private void setAndroidDesiredCapabilities() {
        getDesiredCapabilities().setCapability(PLATFORM_NAME, "Android");
        getDesiredCapabilities().setCapability(PLATFORM_VERSION, "8.0");
        getDesiredCapabilities().setCapability(AUTOMATION_NAME, "UiAutomator2");
        getDesiredCapabilities().setCapability("appPackage", "com.reeftechnology.reef.mobile");
        getDesiredCapabilities().setCapability("appActivity", "io.frameview.reefmobile.httry1.IntroActivity");
        getDesiredCapabilities().setCapability(APP, getAndroidAppFile());
    }

    /**
     * Set IOS Local Desired Capabilities
     */
    private void setIOSLocalDesiredCapabilities() {
        getDesiredCapabilities().setCapability(PLATFORM_NAME, "iOS");
        getDesiredCapabilities().setCapability(DEVICE_NAME, "iPhone 8");
        getDesiredCapabilities().setCapability(PLATFORM_VERSION, "11.3");
        getDesiredCapabilities().setCapability(AUTOMATION_NAME, "XCUITest");
        getDesiredCapabilities().setCapability(FULL_RESET, "true");
        getDesiredCapabilities().setCapability(APP, getIOSAppFile());
    }

    /**
     * Set IOS remote desired capabilities
     */
    private void setIOSRemoteDesiredCapabilities() {
        getDesiredCapabilities().setCapability(DEVICE_NAME, "iPhone 8");
        getDesiredCapabilities().setCapability(UDID, "35C19AD3-C4D1-4F28-B523-68A6D0AD5794");
        getDesiredCapabilities().setCapability(PLATFORM_NAME, "iOS");
        getDesiredCapabilities().setCapability(PLATFORM_VERSION, "11.3");
        getDesiredCapabilities().setCapability(AUTOMATION_NAME, "XCUITest");
        getDesiredCapabilities().setCapability(FULL_RESET, "true");
        getDesiredCapabilities().setCapability(APP, getIOSAppFile());
    }

    private Grid getGrid(@Nullable final String grid) {
        if (!StringUtils.isBlank(grid)) {
            return Grid.valueOf(grid.toUpperCase());
        } else {
            return Grid.NONE;
        }
    }

    /**
     * Tear down driver
     */
    public void tearDown() {
        log.info("Closing Driver for platform: {}.", platform);
        if (null != getDriver()) {
            getDriver().quit();
            driver.remove();
            log.info("Driver closed.");
        } else {
            throw new MobileAutomationException("In tearDown, driver is null!");
        }
    }
}