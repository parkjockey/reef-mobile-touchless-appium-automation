package com.reef.mobile.automation.ui.pages;

import com.reef.mobile.automation.ui.base.BasePage;
import com.reef.mobile.automation.ui.base.BaseDriver;
import io.appium.java_client.MobileBy;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class ReefInitialPage extends BasePage {

    // Android locators
    final By androidReefInitialPageId = By.id("com.reeftechnology.reef.mobile:id/signup_splash_container");
    final By androidLogInButton = By.id("com.reeftechnology.reef.mobile:id/signup_start_login");

    // iOS locators
    final By iosLogInButton = MobileBy.AccessibilityId("Log in");

    public ReefInitialPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    // ********** Android methods **********
    public boolean isAndroidReefInitialPageLoaded() {
        return waitForElementToBeDisplayed(androidReefInitialPageId);
    }

    public void clickAndroidLogInButton() {
        waitAndClick(androidLogInButton);
        log.info("User clicked log in button.");
    }

    // ********** iOs methods **********
    public boolean isIosReefInitialPageLoaded() {
        final WebElement iosReefInitialPageId = driver.findElement(MobileBy.AccessibilityId("splashSignupScreen"));
        return iosReefInitialPageId.getAttribute("name").equals("splashSignupScreen");
    }

    public void clickIosLogInButton() {
        waitAndClick(iosLogInButton);
        log.info("User clicked log in button.");
    }
}