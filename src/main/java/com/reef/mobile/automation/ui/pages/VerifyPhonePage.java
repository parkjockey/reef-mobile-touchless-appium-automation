package com.reef.mobile.automation.ui.pages;

import com.reef.mobile.automation.ui.base.BasePage;
import com.reef.mobile.automation.ui.base.BaseDriver;
import io.appium.java_client.MobileBy;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class VerifyPhonePage extends BasePage {

    // Android locators
    final By androidVerificationPageId = By.id("com.reeftechnology.reef.mobile:id/verify_phone_explanation");
    // TODO: Check how to get verification code from the screen
    final By androidVerificationCodeField = By.id("com.reeftechnology.reef.mobile:id/verify_phone_phone_verification");
    final By androidVerifyPhoneButton = By.id("com.reeftechnology.reef.mobile:id/verify_phone_button");

    // iOS locators
    final By iosVerificationPageId = MobileBy.AccessibilityId("Verify your mobile phone");
    final By iosVerificationCodeFiled = MobileBy.AccessibilityId("VerifyPhonePage_verifyPhoneTextField");

    public VerifyPhonePage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    // ********** Android methods **********
    public boolean isAndroidVerificationPageLoaded() {
        return waitForElementToBeDisplayed(androidVerificationPageId);
    }

    public void enterAndroidVerificationCode(final String verificationCode) {
        waitAndSendKeys(androidVerificationCodeField, verificationCode);
        waitAndClick(androidVerifyPhoneButton);
    }

    // ********** iOS methods **********
    public boolean isIosVerificationPageLoaded() {
        return waitForElementToBeDisplayed(iosVerificationPageId);
    }

    public void enterIosVerificationCode(final String verificationCode) {
        waitAndSendKeys(iosVerificationCodeFiled, verificationCode);
    }
}