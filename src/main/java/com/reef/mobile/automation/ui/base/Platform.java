package com.reef.mobile.automation.ui.base;

public enum Platform {

    ANDROID,
    IOS;
}
