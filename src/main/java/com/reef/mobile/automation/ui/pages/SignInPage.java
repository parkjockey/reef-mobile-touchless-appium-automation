package com.reef.mobile.automation.ui.pages;

import com.reef.mobile.automation.ui.base.BasePage;
import com.reef.mobile.automation.ui.base.BaseDriver;
import io.appium.java_client.MobileBy;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class SignInPage extends BasePage {

    // Android locators
    final By androidSignInPageId = By.id("com.reeftechnology.reef.mobile:id/login_title");
    final By androidEmailField = By.id("com.reeftechnology.reef.mobile:id/email");
    final By androidPasswordField = By.id("com.reeftechnology.reef.mobile:id/password");
    final By androidSignInButton = By.id("com.reeftechnology.reef.mobile:id/login_button");

    // iOS locators
    final By iosSignInPageId = MobileBy.AccessibilityId("Log in to your account");
    final By iosEmailField = MobileBy.AccessibilityId("LoginPage_emailTextField");
    final By iosPasswordField = MobileBy.AccessibilityId("LoginPage_passwordTextField");
    final By iosSignInButton = MobileBy.AccessibilityId("Log In");

    public SignInPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    // ********** Android methods **********
    public boolean isAndroidSignInPageLoaded() {
        return waitForElementToBeDisplayed(androidSignInPageId);
    }

    public void enterAndroidEmailAndPassword(final String email, final String password) {
        waitAndSendKeys(androidEmailField, email);
        waitAndSendKeys(androidPasswordField, password);
        log.info("User entered email: {} and password: {}.", email, password);
    }

    public void clickAndroidSignInButton() {
        waitAndClick(androidSignInButton);
        log.info("User clicked sign in button.");
    }

    // ********** iOS methods **********
    public boolean isIosSignInPageLoaded() {
        return waitForElementToBeDisplayed(iosSignInPageId);
    }

    public void enterIosEmailAndPassword(final String email, final String password) {
        waitAndClick(iosEmailField);
        waitAndSendKeys(iosEmailField, email);
        waitAndClick(iosPasswordField);
        waitAndSendKeys(iosPasswordField, password);
        log.info("User entered email: {} and password: {}.", email, password);
    }

    public void clickIosSingInButton() {
        waitAndClick(iosSignInButton);
        log.info("User clicked sign in button.");
    }
}