package com.reef.mobile.automation.ui.pages;

import com.reef.mobile.automation.ui.base.BasePage;
import com.reef.mobile.automation.ui.base.BaseDriver;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class HomePage extends BasePage {

    final By androidToolbarPageId = By.id("com.reeftechnology.reef.mobile:id/toolbar");

    public HomePage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isAndroidHomePageLoaded() {
        return waitForElementToBeDisplayed(androidToolbarPageId);
    }
}