package com.reef.mobile.automation.storage.scenario;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ScenarioEntity {

    private String scenarioName;
}
