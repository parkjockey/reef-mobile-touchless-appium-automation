package com.reef.mobile.automation.storage;

import com.reef.mobile.automation.storage.scenario.ScenarioEntity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Setter
@Getter
@Component
@Scope("cucumber-glue")
public class Storage {

    private ScenarioEntity scenarioEntity = new ScenarioEntity();
}
