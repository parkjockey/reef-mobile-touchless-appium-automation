Feature: Login

  User is able to login with valid credentials

  @ui @android
  Scenario: Android User logs in with valid credentials
    Given [A] User clicked on login button
    When [A] Signs in with valid credentials
    And [A] Enters verification code
    Then [A] Will be logged in successfully

  @ui @ios
  Scenario: iOS User logs in with valid credentials
    Given [i] User clicked on login button
    When [i] Signs in with valid credentials
    And [i] Enters verification code
    Then [i] Will be logged in successfully