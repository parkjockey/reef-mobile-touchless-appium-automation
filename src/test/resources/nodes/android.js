{
	"capabilities": [
		{
			"deviceName": "AOSP on IA Emulator",
			"UDID": "localhost:55328",
			"version": "8.0",
			"maxInstances": 1,
			"platformName": "Android",
			"browserName": "Android"
		}
	],
	"configuration": {
		"cleanUpCycle": 2000,
		"timeout": 30000,
		"proxy": "org.openqa.grid.selenium.proxy.DefaultRemoteProxy",
		"url": "http://localhost:4723/wd/hub",
		"maxSession": 1,
		"port": "4723",
		"host": "localhost",
		"register": true,
		"registerCycle": 5000,
		"hubPort": "4444",
		"hubHost": "172.17.0.1"
	}
}