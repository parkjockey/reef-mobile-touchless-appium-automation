package com.reef.mobile.automation.hooks;

import com.reef.mobile.automation.exceptions.MobileAutomationException;
import com.reef.mobile.automation.ui.base.BaseDriver;
import com.reef.mobile.automation.storage.Storage;
import com.reef.mobile.automation.config.SpringConfig;
import com.reef.mobile.automation.storage.scenario.ScenarioEntity;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = { SpringConfig.class })
public class Hooks {

    @Autowired
    private Storage storage;

    @Autowired
    private BaseDriver baseDriver;

    final Logger logger = LoggerFactory.getLogger(Hooks.class);

    @Before(order = 0)
    public void scenarioStarted(final Scenario scenario) {
        logger.info("SCENARIO: {} started!", scenario.getName());
        final ScenarioEntity testScenario = storage.getScenarioEntity();
        testScenario.setScenarioName(scenario.getName());
    }

    @Before(value = "@ui", order = 1)
    public void setupDriver(final Scenario scenario) {
        if (scenario.getSourceTagNames().contains("@android")) {
            baseDriver.initializeAndroidWebDriver();
        } else if (scenario.getSourceTagNames().contains("@ios")) {
            baseDriver.initializeIOSWebDriver();
        } else {
            throw new MobileAutomationException("Scenario must contain tag @android or @ios, check your scenario!");
        }
    }

    @After(order = 0)
    public void scenarioFinished(final Scenario scenario) {
        logger.info("SCENARIO: '{}' completed with status '{}'", scenario.getName(), scenario.getStatus());
    }

    @After(value = "@ui", order = 1)
    public void tearDown() {
        baseDriver.tearDown();
    }
}