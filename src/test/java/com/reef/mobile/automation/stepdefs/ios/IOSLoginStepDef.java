package com.reef.mobile.automation.stepdefs.ios;

import com.reef.mobile.automation.ui.pages.HomePage;
import com.reef.mobile.automation.ui.pages.ReefInitialPage;
import com.reef.mobile.automation.ui.pages.SignInPage;
import com.reef.mobile.automation.ui.pages.VerifyPhonePage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

public class IOSLoginStepDef {

    @Autowired
    private ReefInitialPage reefInitialPage;

    @Autowired
    private SignInPage signInPage;

    @Autowired
    private VerifyPhonePage verifyPhonePage;

    @Autowired
    private HomePage homePage;

    @Given("[i] User clicked on login button")
    public void i_User_clicked_on_login_button() {
        assertThat(reefInitialPage.isIosReefInitialPageLoaded()).as("Reef initial page is not loaded!").isTrue();
        reefInitialPage.clickIosLogInButton();
    }

    @When("[i] Signs in with valid credentials")
    public void signInWithValidCredentials() {
        assertThat(signInPage.isIosSignInPageLoaded()).as("Sign in page is no loaded!").isTrue();

        signInPage.enterIosEmailAndPassword("test@gomail5.com", "Sifra123!");
        signInPage.clickIosSingInButton();
    }

    @When("[i] Enters verification code")
    public void entersVerificationCode() {
        assertThat(verifyPhonePage.isIosVerificationPageLoaded()).as("Verification page did not open!").isTrue();
    }

    @Then("[i] Will be logged in successfully")
    public void loggedInSuccessfully() {
    }
}