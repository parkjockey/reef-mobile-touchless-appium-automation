package com.reef.mobile.automation.stepdefs.android;

import com.reef.mobile.automation.ui.pages.HomePage;
import com.reef.mobile.automation.ui.pages.ReefInitialPage;
import com.reef.mobile.automation.ui.pages.SignInPage;
import com.reef.mobile.automation.ui.pages.VerifyPhonePage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import static org.assertj.core.api.Assertions.*;

public class AndroidLoginStepDef {

    @Autowired
    private ReefInitialPage reefInitialPage;

    @Autowired
    private SignInPage signInPage;

    @Autowired
    private VerifyPhonePage verifyPhonePage;

    @Autowired
    private HomePage homePage;

    @Given("[A] User clicked on login button")
    public void clickLoginButton() {
        assertThat(reefInitialPage.isAndroidReefInitialPageLoaded()).as("Reef initial page is not loaded!").isTrue();
        reefInitialPage.clickAndroidLogInButton();
    }

    @When("[A] Signs in with valid credentials")
    public void signInWithValidCredentials() {
        assertThat(signInPage.isAndroidSignInPageLoaded()).as("Sign in page is no loaded!").isTrue();

        signInPage.enterAndroidEmailAndPassword("test@gomail5.com", "Sifra123!");
        signInPage.clickAndroidSignInButton();
    }

    @When("[A] Enters verification code")
    public void entersVerificationCode() {
        assertThat(verifyPhonePage.isAndroidVerificationPageLoaded()).as("Verification page did not open!").isTrue();
    }

    @Then("[A] Will be logged in successfully")
    public void loggedInSuccessfully() {
    }
}